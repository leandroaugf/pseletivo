/*
Para criar uma nova branch, o comando a seguir deveria ser digitado no terminal: git checkout -b "newbranch/feature"
Onde "newbranch/feature" representa o nome da nova branch;
A função de divisão, desenvolvida, seria a seguinte: 
*/

const soma = () => {
  console.log(parseInt(args[0]) + parseInt(args[1]));
};

const sub = () => {
  console.log(parseInt(args[0]) - parseInt(args[1]));
};
const div = () => {
  console.log(parseInt(args[0]) / parseInt(args[1]));
};

const args = process.argv.slice(2);

switch (args[0]) {
  case "soma":
    soma();
    break;

  case "sub":
    sub();
    break;

  default:
    console.log("does not support", args[0]);
}
